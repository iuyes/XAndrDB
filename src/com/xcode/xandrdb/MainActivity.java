package com.xcode.xandrdb;

import com.xcode.xandrdb.Factory.SessionConfig;
import com.xcode.xandrdb.Factory.SessionFactory;
import com.xcode.xandrdb.interfaces.Session;
import com.xcode.xandrdb.mapper.test;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Session session = SessionFactory.getSession(new SessionConfig().setDBName("xiaolei"));
		test t1 = session.getMapper(test.class);
		User users[] = t1.select(1);
		System.out.println(users);
	}
}
