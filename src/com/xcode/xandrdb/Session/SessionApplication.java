package com.xcode.xandrdb.Session;

import java.util.HashMap;
import java.util.Map;
import com.xcode.xandrdb.Factory.HandlerFactory;
import com.xcode.xandrdb.interfaces.Session;

import android.app.Application;

public class SessionApplication extends Application implements Session
{
	private static SessionApplication mSession = null;

	@Override
	public void onCreate()
	{
		super.onCreate();
		mSession = this;
	}

	public static SessionApplication getInstance()
	{
		return mSession;
	}

	private Map<Class<?>, HandlerFactory> factorys = new HashMap<Class<?>, HandlerFactory>();

	/**
	 * 根据传入的类型，寻找对应的InvocationHandler
	 */
	@Override
	public <T> T getMapper(Class<T> type)
	{
		HandlerFactory factory = factorys.get(type);
		// 如果获取到的对象是null则给他新建一个 然后再保存
		if (factory == null)
		{
			factory = new HandlerFactory(type);
			factorys.put(type, factory);
		}
		return factory.newInstance();
	}
}
